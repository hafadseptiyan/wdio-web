# Webdriver.io Web Automation

#### Install dependencies

Open your code editor, and send this command on your directory

``` sh
npm install
```

#### Set your PrivyId and Password

On otp.e2e.js

change const `privyId` and `password` using your valid Privy Account.


#### Run test

``` sh
npm run wdio
```
