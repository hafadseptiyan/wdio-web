

import Page from './page.js';
const base_url = "https://app.privy.id"

let method;

/**
 * sub page containing specific selectors and methods for a specific page
 */
class LoginPage extends Page {
    /**
     * define selectors using getter methods
     */
    get inputUsername () {
        return $('input[name="user[privyId]"]');
    }
   
    get inputPassword () {
        return $('input[name="user[secret]"]');
    }

    get btnSubmit () {
        return $('button[type="submit"]');
    }

    /**
     * a method to encapsule automation code to interact with the page
     * e.g. to login using username and password
     */
    async login (username, password) {
        await this.inputUsername.setValue(username)
        await this.btnSubmit.click()
        await this.inputPassword.setValue(password)
        await this.btnSubmit.click()

        await expect(browser).toHaveUrlContaining("dashboard")
        
        await browser.url(base_url + '/settings/change-password')
        await browser.$('#v-security-0__BV_toggle_').click()

        const currentMethod = await browser.$('#v-security-0__BV_toggle_').getText()
        if(currentMethod === "Send OTP via SMS") {
            method = "Email"
        } else {
            method = "SMS"
        }
        await browser.$("aria/Send OTP via " + method).click()
        await browser.refresh()
    }

    /**
     * overwrite specific options to adapt it to page object
     */
    open () {
        return super.open('login');
    }
}

export default new LoginPage();
